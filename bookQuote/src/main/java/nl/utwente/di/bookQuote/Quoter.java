package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {
    Map<String, Double> isbns = new HashMap<>();
    public double getBookPrice(String name) {
        isbns.put("1" ,10.0);
        isbns.put("2" ,45.0);
        isbns.put("3" ,20.0);
        isbns.put("4" ,35.0);
        isbns.put("5" ,50.0);
        return isbns.getOrDefault(name, 0.0);
    }
}
